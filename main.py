#pg skelett

import pygame as pg
import sys
import random
import os
import sqlite3
from tilemap import *
from config import *
from sprites import *

def mil_to_min(miliseconds):
    return round(miliseconds / 1000 / 60, 2)

class PopUp():
    def __init__(self):
        global NMBRS
        check = False
        self.number = random.randrange(1, 13)
        while not check:
            if self.number in NMBRS:
                self.number = random.randrange(1, 13)
            else:
                NMBRS.append(self.number)
                check = True
        self.img = pg.image.load(os.path.join(IMG_FOLDER, 'fossil'+str(self.number)+'.png'))
        self.rect = self.img.get_rect()

class Game():
    def __init__(self):
        # SQL conntection
        self.conn = sqlite3.connect('highscores.db')
        self.c = self.conn.cursor()
        # pg initialisieren und fenster erstellen
        pg.init()
        self.screen = pg.display.set_mode((WIDTH, HEIGHT))
        pg.display.set_caption(TITLE)
        self.initGame(LEVEL_ONE)
        #classvars
        self.playerName = ""
        self.controlsMenu = pg.image.load(os.path.join(IMG_FOLDER, 'control_hilfe.png')).convert_alpha()

    def intro(self):
        time = 8
        fadeTime = time/2
        '''Anzeige des Spielintros'''
        backround = pg.image.load(os.path.join(IMG_FOLDER, START_MENU))
        introscreen = pg.surface.Surface((WIDTH, HEIGHT))
        introscreen.blit(backround, (0,0))
        count = 0
        
        title = self.text_format("Fossil Hunter", 60, WHITE)
        selected= "play"
        intro1 = self.text_format('Einst machte sich ein mutiger Abenteurer', 30, WHITE) 
        intro2 = self.text_format('auf den Weg eine unbekannte Hoehle', 30, WHITE) 
        intro3 = self.text_format('zu erkunden...', 30, WHITE)

        intro4 = self.text_format('Dort angekommen', 30, WHITE)
        intro5 = self.text_format('wurde der Eingang', 30, WHITE)
        intro6 = self.text_format('zur Höhle verschüttet...', 30, WHITE)

        intro7 = self.text_format('Mit seiner Oellampe in der Hand', 30, WHITE)
        intro8 = self.text_format('sucht er nach den Geheimnissen der Höhle', 30, WHITE)
        intro9 = self.text_format('und einem Ausgang...', 30, WHITE)

        intro10 = self.text_format('Der Name dieses Abenteurers ist...', 30, WHITE)
        
        clock = pg.time.Clock()
        timer = 0

        
        while True:
            timer += 1
            seconds = timer/FPS
            clock.tick(FPS)
            introscreen.blit(backround, (0,0))
            for event in pg.event.get():
                if event.type==pg.QUIT:
                    return pg.quit()

                if event.type==pg.KEYDOWN:
                    if event.key==pg.K_UP:
                        selected="play"
                    elif event.key==pg.K_DOWN:
                        selected="quit"
                    if event.key==pg.K_RETURN:
                        return selected
                            
     
            if selected=="play":
                text_skip = self.text_format("->SKIP", 30, GREEN)
            else:
                text_skip = self.text_format("SKIP", 30, WHITE)
            if selected=="quit":
                text_quit=self.text_format("->QUIT", 30, RED)
            else:
                text_quit = self.text_format("QUIT", 30, WHITE)
     
            title_rect=title.get_rect()
            skip_rect=text_skip.get_rect()
            quit_rect=text_quit.get_rect()

 
            

            if seconds < fadeTime  or seconds > fadeTime*2 and seconds < fadeTime*3 or seconds > fadeTime*4 and seconds \
                < fadeTime*5 or seconds > fadeTime*6 and seconds  < fadeTime*7 :
                if count < 255:
                    count += 3
            elif seconds > fadeTime and seconds < fadeTime*2 or seconds  > fadeTime*3 and seconds < fadeTime*4 or seconds \
                 > fadeTime*5 and seconds < fadeTime*6 or seconds > fadeTime*7:
                if count >=0:
                    count -=3
            
     
            introscreen.blit(title, (WIDTH/2 - (title_rect.width/2), 50))
            introscreen.blit(text_skip, (WIDTH/2 - (skip_rect.width/2), 430))
            introscreen.blit(text_quit, (WIDTH/2- (quit_rect.width/2), 470))
            if seconds < time:
                intro1.set_alpha(count)
                intro2.set_alpha(count)
                intro3.set_alpha(count) 
                introscreen.blit(intro1, (WIDTH/2 - (intro1.get_rect().width/2), 250))
                introscreen.blit(intro2, (WIDTH/2 - (intro2.get_rect().width/2), 300))
                introscreen.blit(intro3, (WIDTH/2 - (intro3.get_rect().width/2), 350))
            elif seconds < time*2:
                intro4.set_alpha(count)
                intro5.set_alpha(count)
                intro6.set_alpha(count)
                introscreen.blit(intro4, (WIDTH/2 - (intro4.get_rect().width/2), 250))
                introscreen.blit(intro5, (WIDTH/2 - (intro5.get_rect().width/2), 300))
                introscreen.blit(intro6, (WIDTH/2 - (intro6.get_rect().width/2), 350))
            elif seconds < time*3:
                intro7.set_alpha(count)
                intro8.set_alpha(count)
                intro9.set_alpha(count)
                introscreen.blit(intro7, (WIDTH/2 - (intro7.get_rect().width/2), 250))
                introscreen.blit(intro8, (WIDTH/2 - (intro8.get_rect().width/2), 300))
                introscreen.blit(intro9, (WIDTH/2 - (intro9.get_rect().width/2), 350))
            elif seconds < time*4- fadeTime/2:
                intro10.set_alpha(count)
                introscreen.blit(intro10, (WIDTH/2 - (intro10.get_rect().width/2), 300))
            else :
                return "play"
            
                
            
            self.screen.blit(introscreen, (0, 0))
            
            pg.display.flip()

    def displayMenu(self):
        '''Anzeige des Hauptmenüs'''
        background = pg.image.load(os.path.join(IMG_FOLDER, START_MENU))
        overlay = pg.Surface((WIDTH, HEIGHT))
        started = False
        inp = ""
        selected = "name"
        count = 255
        displayControls = False

        while not started:
            for event in pg.event.get():
                # überprüfen, ob das fenster geschlossen wurde
                if event.type == pg.QUIT:
                    started = True
                    return pg.quit()
                if event.type==pg.KEYDOWN:
                    if event.key==pg.K_DOWN:
                        selected="controls"

                    if selected == "controls":
                        if event.key == pg.K_RETURN:
                            displayControls = True

                        if event.key == pg.K_ESCAPE:
                            displayControls = False

                    if event.key==pg.K_UP:
                        selected="name"
                    if selected == "name":
                        if event.key == pg.K_BACKSPACE:
                            inp = inp[0:-1]
                        if event.key != pg.K_BACKSPACE and event.key != pg.K_RETURN:
                            inp += event.unicode
                        if event.key == pg.K_RETURN and inp and not inp.isspace():
                            started = True
                            self.playerName = inp.strip()
                            return True

            if count >= 2:
                count -= 2

            if selected== "name":
                name_text = "-> " + inp
                name_color = ORANGE
            else:
                name_text = inp
                text_restart = WHITE
            if selected == "controls":
                controls_text = "-> SPIELSTEUERUNG"
                controls_color = GREEN
            else:
                controls_text = "SPIELSTEUERUNG"
                controls_color = WHITE

            if not displayControls: 
                overlay.set_alpha(count)
                self.screen.blit(background, (0,0))
                self.display_text("MENÜ", 60, WHITE, WIDTH/2, HEIGHT*0.2)
                self.display_text("Bitte Namen eingeben und Enter drücken:", 40, WHITE, WIDTH/2, HEIGHT*0.3)
                self.display_text(name_text, 40, name_color, WIDTH/2, HEIGHT*0.4)
                self.display_text(controls_text, 40, controls_color, WIDTH/2, HEIGHT*0.6)
                self.screen.blit(overlay, (0,0))
            else: 
                self.screen.blit(background, (0,0))
                self.screen.blit(self.controlsMenu, (0,0))

            pg.display.flip()

    def initGame(self, mapfile):
        ''' Laden der Spielinhalte '''
        self.displayFossil = False
        self.clock = pg.time.Clock() # Spielzeit
        pg.mixer.pre_init(44100,16,2,4096) # sound
        pg.mixer.music.set_volume(0.3)
        pg.mixer.set_num_channels(8)
        pg.mixer.Channel(0).play(pg.mixer.Sound('Sound/End_of_Monolith_filled.ogg'),-1,0,0)
        # Map und Kamera initialisieren
        self.game_map = TiledMap(os.path.join(MAP_FOLDER, mapfile))
        self.map_img = self.game_map.make_map()
        self.map_rect = self.map_img.get_rect()
        self.camera = Camera(self.game_map.width, self.game_map.height)

        self.initSprites()

    def initSprites(self):
        ''' Initialisierung der Sprite-(gruppen) '''
        self.walls = pg.sprite.Group()
        self.oil = pg.sprite.Group()
        self.fossils = pg.sprite.Group()
        self.playerSprite = pg.sprite.Group();
        self.all_sprites = pg.sprite.Group() #grafiken
        self.exit = pg.sprite.Group()
        self.light=Light()
        self.parseTmx()
        
    def parseTmx(self):
        ''' Parsen der TMX Datei und lsesen der Spielobjekte'''
        for tile_object in self.game_map.tmxdata.objects:
            if tile_object.name == 'player':
                self.player = Player(tile_object.x, tile_object.y, self.playerSprite, self.walls)
                self.all_sprites.add(self.player)
            if tile_object.name == 'wall':
                self.walls.add(Obstacle(tile_object.x, tile_object.y, tile_object.width, tile_object.height, self.walls))
            if tile_object.name == 'oil':
                self.oil.add(Object(tile_object.x, tile_object.y, self.oil, OIL))
            if tile_object.type == 'fossil':
                self.fossils.add(Object(tile_object.x, tile_object.y, self.fossils, FOSSIL))
            if tile_object.name == 'exit':
                self.exit.add(Obstacle(tile_object.x, tile_object.y,tile_object.width, tile_object.height, self.exit))

    def drawSpriteGroup(self, group):
        ''' Zeichnen einer Spritegruppen '''
        for item in group:
            self.screen.blit(item.image, self.camera.apply(item))

    def drawPopUp(self):
        ''' Anzeige des Fossil-PopUps '''
        self.popUp = PopUp()
        pg.mixer.Channel(0).pause()
        pg.mixer.Channel(2).play(pg.mixer.Sound('Sound/'+ 'sound' +str(self.popUp.number) +'.ogg'),0,0,0)
        self.displayFossil = False
        self.timer = pg.time.get_ticks() - self.offset
        self.loop = True

        while self.loop:
            self.screen.blit(self.popUp.img, (WIDTH/2-self.popUp.rect.width/2, HEIGHT/2-self.popUp.rect.height/2))
            if (pg.time.get_ticks() - self.offset) - self.timer >= POPUP_TIME:
                self.loop = False
            for event in pg.event.get():
                if event.key == pg.K_RETURN:
                    self.loop = False
            pg.display.update()

        pg.mixer.Channel(0).unpause()    
        self.light.reSize(self.offset)

    def text_format(self, message, textSize, textColor):
        ''' Gibt ein Textsufaceobjekt zurück '''
        font = pg.font.Font('freesansbold.ttf', textSize)
        textSurface = font.render(message, False, textColor, BLUE)
        textSurface.set_colorkey(BLUE)
        return textSurface

    def win_screen(self):
        ''' Anzeige des Menüs nach erfolgreichem Spielende '''
        global NMBRS
        time_score = mil_to_min(pg.time.get_ticks()-self.offset)
        fossils_score = len(NMBRS)
        self.saveHighscore(fossils_score, time_score)
        menu = True
        selected= "restart"
        background = pg.image.load(os.path.join(IMG_FOLDER, WIN_MENU))
        highscores = self.getHighscores()
        title = self.text_format("Nice, " + self.playerName + "!", 80, BLUE)
        items = self.text_format("Items: " + str(fossils_score) +"/7", 50, GREEN)
        time = self.text_format("Zeit: " + str(time_score), 50, GREEN)
        highscoretext = self.text_format("Highscore", 50, RED)

        while menu:
            for event in pg.event.get():
                if event.type==pg.QUIT:
                   return pg.quit()

                if event.type==pg.KEYDOWN:
                    if event.key==pg.K_LEFT:
                        selected="restart"
                    elif event.key==pg.K_RIGHT:
                        selected="quit"
                    if event.key==pg.K_RETURN:
                        if selected == "restart":
                            self.initGame(LEVEL_ONE)
                            NMBRS = []
                            return self.gameLoop()
                        if selected == "quit":
                            return pg.quit()
 
            if selected== "restart":
                text_restart = self.text_format("RESTART", 60, ORANGE)
            else:
                text_restart = self.text_format("RESTART", 60, WHITE)
            if selected == "quit":
                text_quit = self.text_format("QUIT", 60, RED)
            else:
                text_quit = self.text_format("QUIT", 60, WHITE)
     
            title_rect=title.get_rect()
            restart_rect=text_restart.get_rect()
            quit_rect=text_quit.get_rect()
            items_rect=items.get_rect()
            time_rect=time.get_rect()
            highscoretext_rect=highscoretext.get_rect()

            self.screen.blit(background, (0,0))
            self.screen.blit(title, (WIDTH/2 - (title_rect.width/2), 50))
            self.screen.blit(items, (WIDTH*(1/3) - (items_rect.width/2), 150))
            self.screen.blit(time, (WIDTH*(2/3) - (time_rect.width/2), 150))
            self.screen.blit(highscoretext, (WIDTH/2 - (highscoretext_rect.width/2), 200))
            self.screen.blit(text_restart, (WIDTH/3 - (restart_rect.width/2), 500))
            self.screen.blit(text_quit, (WIDTH*(2/3)- (quit_rect.width/2), 500))
            pos = 280
            self.display_text("name", 20, WHITE, WIDTH*0.4, pos)
            self.display_text("# fossils", 20, WHITE, WIDTH*0.5, pos)
            self.display_text("time", 20, WHITE, WIDTH*0.6, pos)
            for score in highscores:
                pos += 30
                self.display_text(score[0], 20, WHITE, WIDTH*0.4, pos)
                self.display_text(str(score[1]), 20, WHITE, WIDTH*0.5, pos)
                self.display_text(str(score[2]), 20, WHITE, WIDTH*0.6, pos) 
            pg.display.update()

    def end_screen(self):
        ''' Zeigt einen Bildschirm an in dem ausgewählt werden kann ob das Spiel wiederholt (True) oder beendet (False) wird.'''
        global NMBRS
        menu=True
        selected="restart"
        # End Screen UI
        background = pg.image.load(os.path.join(IMG_FOLDER, START_MENU))
        title = self.text_format(self.playerName, 80, RED)

        while menu:
            for event in pg.event.get():
                if event.type==pg.QUIT:
                   return pg.quit()

                if event.type==pg.KEYDOWN:
                    if event.key==pg.K_UP:
                        selected="restart"
                    elif event.key==pg.K_DOWN:
                        selected="quit"
                    if event.key==pg.K_RETURN:
                        if selected == "restart":
                            NMBRS = []
                            self.initGame(LEVEL_ONE)
                            return self.gameLoop()
                        if selected == "quit":
                            return pg.quit()
                        
 
            if selected=="restart":
                text_restart = self.text_format("RESTART", 60, WHITE)
            else:
                text_restart = self.text_format("RESTART", 60, BLACK)
            if selected=="quit":
                text_quit=self.text_format("QUIT", 60, WHITE)
            else:
                text_quit = self.text_format("QUIT", 60, BLACK)
     
            title_rect=title.get_rect()
            restart_rect=text_restart.get_rect()
            quit_rect=text_quit.get_rect()
     
            # End Screen Text
            self.screen.blit(background, (0,0))
            self.screen.blit(title, (WIDTH/2 - (title_rect.width/2), 100))
            self.screen.blit(text_restart, (WIDTH/2 - (restart_rect.width/2), 300))
            self.screen.blit(text_quit, (WIDTH/2- (quit_rect.width/2), 400))
            
            pg.display.update()
            
    def display_text(self, text, size, color, posX, posY):
        ''' Anzeige eines Textes zu einer gegebenen Position '''
        font = pg.font.Font('freesansbold.ttf', size)
        textSurface = font.render(text, True, color)
        textRect = textSurface.get_rect()
        textRect.center = (posX, posY)
        self.screen.blit(textSurface, textRect)

    def update(self):
        ''' Aktualisierung der Spielzustände '''
        if pg.sprite.groupcollide(self.playerSprite, self.oil, False, True):
            pg.mixer.init(44100,16,2,4096)
            pg.mixer.Channel(1).play(pg.mixer.Sound('Sound/oilcollide.ogg'),0,0,0)
            self.light.reSize(self.offset)

        if pg.sprite.groupcollide(self.playerSprite, self.fossils, False, True):
            self.drawPopUp()
            self.light.reSize(self.offset)

    def saveHighscore(self, fossils, time):
        ''' Eintragung des aktuellen Highscores '''
        self.c.execute('SELECT * FROM highscores WHERE name=?', (self.playerName,))
        data = self.c.fetchone()
        if data is None:
            self.c.execute('INSERT INTO highscores (name, fossils, time) VALUES (?, ?, ?)', (self.playerName, fossils, time))
            self.conn.commit()
        else:
            if (data[2] < fossils or (data[2] == fossils and data[3] > time)):
                self.c.execute('UPDATE highscores SET time = ?, fossils = ? WHERE id = ?', (time, fossils, data[0]))
                self.conn.commit()

    def getHighscores(self):
        ''' Abfrage der top 3 Highscores '''
        arr = []
        for row in self.c.execute('SELECT name, fossils, time FROM highscores ORDER BY fossils DESC, time ASC LIMIT 3'):
            arr.append(row)
        return arr

    def gameLoop(self):
        ''' Gameloop '''
        self.running = True
        self.offset = pg.time.get_ticks()

        # gameloop ausführen solange running = true
        while self.running:
            # geschwindigkeit des gameloops
            self.clock.tick(FPS)

            # Gameloop = 1. Input -> 2. Update -> 3. Draw
            # 1. Input (events)
            for event in pg.event.get():
                # überprüfen, ob das fenster geschlossen wurde
                if event.type == pg.QUIT:
                    self.running = False
                    return pg.quit()

            # 2. Update
            self.all_sprites.update()
            self.camera.update(self.player) # Camera funktion von tilemap.py
            self.update()
            self.light.light_ani(self.offset)
            
            # 3. Draw
            self.screen.blit(self.map_img, self.camera.apply_rect(self.map_rect)) #map zeichnen und kamera setzen
            #items zeichnenInDesignInDesign
            self.drawSpriteGroup(self.oil)
            self.drawSpriteGroup(self.fossils)
            #lichteffekt
            filter = pg.surface.Surface((WIDTH, HEIGHT)) # Erst stellt einen surface in der Größe des Fensters
            filter.fill(pg.color.Color('Black')) # Füllt den Surface kommplett schwarz aus
            filter.blit(self.light.image, (self.camera.apply(self.player).x-(self.light.rect.width/2-self.player.rect.width/2), self.camera.apply(self.player).y-self.light.rect.height/2+self.player.rect.width/2)) # lädt das Licht object dazu
            self.screen.blit(filter, (0,0), special_flags=pg.BLEND_RGBA_MIN) # rechnet die beiden Overflächen gegeneinander
            #spieler zeichnen
            self.screen.blit(self.player.image, self.camera.apply(self.player))      

            if pg.sprite.groupcollide(self.playerSprite, self.exit, True, True):
                self.running = False
                return self.win_screen()
                
            if self.light.endState():
                self.running = False
                return self.end_screen()
            
            # kommt immer ans ende
            pg.display.flip() # zeigt das neu gerenderte Bild
        
# Führt den Gameloop solange erneut aus, bis im screen 'Quit' ausgewählt wird        
game = Game()
if game.intro():
    if game.displayMenu():                     
        game.gameLoop()

pg.quit()
