import pygame as pg
import os

# Hier werden alle Konstanten für das Spiel gespeichert

# Spieleinstellungen
WIDTH = 1000
HEIGHT = 600
FPS = 40
TITLE = "Fossil Hunter"
NMBRS = []

TILESIZE = 32
GRIDWIDTH = WIDTH / TILESIZE
GRIDHEIGHT = HEIGHT / TILESIZE

BLACK = (0,0,0)
WHITE = (255, 255, 255)
GREY=(50, 50, 50)
RED=(255, 0, 0)
GREEN=(50,205,50)
BLUE=(21, 173, 215)
YELLOW=(255, 255, 0)
ORANGE=(233, 115, 28)

font = "freesansbold.ttf"

# Ordner und Dateinen
GAME_FOLDER = os.path.dirname(__file__)
IMG_FOLDER = os.path.join(GAME_FOLDER, 'img')
MAP_FOLDER = os.path.join(GAME_FOLDER, 'maps')
SOUND_FOLDER = os.path.join(GAME_FOLDER, 'Sound')
LEVEL_ONE = 'Level_one.tmx'
OIL = 'oil.png'
FOSSIL = 'Fossil.png'
START_MENU = 'start_menu.png'
WIN_MENU = 'win_menu.png'

# Player settings
PLAYER_SPEED = 3
LIGHT_TIME = 4000
POPUP_TIME = 4500
