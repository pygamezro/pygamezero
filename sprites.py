import pygame as pg
from config import *

# Spielerklasse
class Player(pg.sprite.Sprite):

    # grafik (sprite) spieler
    def __init__(self, posx, posy, playerSprite, obstacles):
        pg.sprite.Sprite.__init__(self, playerSprite)
        self.load_images()
        self.image = self.standing[0]
        self.current_frame = 0
        self.last_update = 0
        self.rect = self.image.get_rect() # rechteck um image mappen für positionskontrolle
        self.rect.topleft = (posx, posy) # position festlegen
        self.mask = pg.mask.from_surface(self.image)
        self.displayFossil = False
        self.playerSprite = playerSprite
        self.obstacles = obstacles

        self.dirX = 0
        self.dirY = 0
        
    def load_images(self):
        # Grafiken für das Stehen in einem Array (im Kreis drehen)
        self.standing = [pg.image.load(os.path.join(IMG_FOLDER, 'georgeR1.png')).convert_alpha(),
                         pg.image.load(os.path.join(IMG_FOLDER, 'georgeU1.png')).convert_alpha(),
                         pg.image.load(os.path.join(IMG_FOLDER, 'georgeL1.png')).convert_alpha(),
                         pg.image.load(os.path.join(IMG_FOLDER, 'georgeD1.png')).convert_alpha()]

        # Grafiken für das Laufen nach Rechts in einem Array
        self.walking_r = [pg.image.load(os.path.join(IMG_FOLDER, 'georgeR1.png')).convert_alpha(),
                          pg.image.load(os.path.join(IMG_FOLDER, 'georgeR2.png')).convert_alpha(),
                          pg.image.load(os.path.join(IMG_FOLDER, 'georgeR3.png')).convert_alpha(),
                          pg.image.load(os.path.join(IMG_FOLDER, 'georgeR4.png')).convert_alpha()]
       
        # Grafiken für das Laufen nach Oben in einem Array
        self.walking_u = [pg.image.load(os.path.join(IMG_FOLDER, 'georgeU1.png')).convert_alpha(),
                          pg.image.load(os.path.join(IMG_FOLDER, 'georgeU2.png')).convert_alpha(),
                          pg.image.load(os.path.join(IMG_FOLDER, 'georgeU3.png')).convert_alpha(),
                          pg.image.load(os.path.join(IMG_FOLDER, 'georgeU4.png')).convert_alpha()]

        # Grafiken für das Laufen nach Unten in einem Array
        self.walking_d = [pg.image.load(os.path.join(IMG_FOLDER, 'georgeD1.png')).convert_alpha(),
                          pg.image.load(os.path.join(IMG_FOLDER, 'georgeD2.png')).convert_alpha(),
                          pg.image.load(os.path.join(IMG_FOLDER, 'georgeD3.png')).convert_alpha(),
                          pg.image.load(os.path.join(IMG_FOLDER, 'georgeD4.png')).convert_alpha()]

        # Grafiken für das Laufen nach Links in einem Array
        self.walking_l = []
        for image in self.walking_r:
            self.walking_l.append(pg.transform.flip(image, True, False))
        
        # Animationen für die verschiedenen Richtungen
    def animation(self):
        nowTime = pg.time.get_ticks()
        if self.dirX != 0 or self.dirY != 0:
            self.walking = True
        else:
            self.walking = False

        if self.walking:
            if nowTime - self.last_update > 200:
                self.last_update = nowTime
                self.current_frame = (self.current_frame +1) % len(self.walking_r)
            # Laufen nach rechts
            if self.dirX > 0:
                self.image = self.walking_r[self.current_frame]
            # Laufen nach links
            elif self.dirX < 0:
                self.image = self.walking_l[self.current_frame]
            # Laufen nach unten
            elif self.dirY > 0:
                self.image = self.walking_d[self.current_frame]
            # Laufen nach oben
            elif self.dirY < 0:
                self.image = self.walking_u[self.current_frame]
        
    # animation wenn Spieler stehen bleibt
    def stand_ani(self):
        nowTime = pg.time.get_ticks()
        if nowTime - self.last_update > 1000:
            self.last_update = nowTime
            self.current_frame = (self.current_frame + 1) % len(self.standing)
            self.image = self.standing[self.current_frame]


    # Steuerung Spieler
    def update(self):
        # erkennt kollision 
        keys = pg.key.get_pressed()
        if keys[pg.K_LEFT] or keys[pg.K_a]:
            self.animation()
            self.dirX = -PLAYER_SPEED
            self.dirY = 0
            self.rect.x += self.dirX
        elif keys[pg.K_RIGHT] or keys[pg.K_d]:
            self.animation()
            self.dirY = 0
            self.dirX = PLAYER_SPEED
            self.rect.x += self.dirX
        elif keys[pg.K_UP] or keys[pg.K_w]:
            self.animation()
            self.dirY = -PLAYER_SPEED
            self.dirX = 0
            self.rect.y += self.dirY
        elif keys[pg.K_DOWN] or keys[pg.K_s]:
            self.animation()
            self.dirY = PLAYER_SPEED
            self.dirX = 0
            self.rect.y += self.dirY
        else:
            self.stand_ani()
        

        # Position zurück setzen, falls Kollision mit Wand.
        if pg.sprite.groupcollide(self.playerSprite, self.obstacles, False, False):
            if self.dirX > 0:
                self.rect.x -= PLAYER_SPEED
            if self.dirX < 0:
                self.rect.x += PLAYER_SPEED
            if self.dirY > 0:
                self.rect.y -= PLAYER_SPEED
            if self.dirY < 0:
                self.rect.y += PLAYER_SPEED

class Light(pg.sprite.Sprite):
    def __init__(self):

        self.load_light()

        self.image = self.lightA[0]
        self.current_frame_light = 0
        self.last_update_light = 0

        self.rect = self.image.get_rect()


    def load_light(self):
    # Grafiken für das Kleinerwerden des Sichtkreises
        self.lightA = [pg.image.load(os.path.join(IMG_FOLDER, 'light8.png')).convert_alpha(),
                       pg.image.load(os.path.join(IMG_FOLDER, 'light7.png')).convert_alpha(),
                       pg.image.load(os.path.join(IMG_FOLDER, 'light6.png')).convert_alpha(),
                       pg.image.load(os.path.join(IMG_FOLDER, 'light5.png')).convert_alpha(),
                       pg.image.load(os.path.join(IMG_FOLDER, 'light4.png')).convert_alpha(),
                       pg.image.load(os.path.join(IMG_FOLDER, 'light3.png')).convert_alpha(),
                       pg.image.load(os.path.join(IMG_FOLDER, 'light2.png')).convert_alpha(),
                       pg.image.load(os.path.join(IMG_FOLDER, 'light1.png')).convert_alpha(),
                       pg.image.load(os.path.join(IMG_FOLDER, 'light0.png')).convert_alpha(),
                       pg.image.load(os.path.join(IMG_FOLDER, 'black.png')).convert_alpha()]

    def endState(self):
        if self.current_frame_light == 9:
            return True
        return False
    
    def reSize(self, offset):
        self.current_frame_light = 0
        self.last_update_light = pg.time.get_ticks() -offset
        self.image = self.lightA[self.current_frame_light]
        self.rect = self.image.get_rect()
    
    # Animation für das Licht
    def light_ani(self, offset):
        nowTime = pg.time.get_ticks() - offset
        if nowTime - self.last_update_light > LIGHT_TIME:
            self.last_update_light = nowTime
            self.current_frame_light = (self.current_frame_light + 1)
            self.image = self.lightA[self.current_frame_light]
            self.rect = self.image.get_rect()

    def update(self):
        self.light_ani()

class Object(pg.sprite.Sprite):
    def __init__(self, x, y, group, image):
        pg.sprite.Sprite.__init__(self, group)
        self.image = pg.image.load(os.path.join(IMG_FOLDER, image)).convert_alpha()
        self.rect = self.image.get_rect() # rechteck um image mappen für positionskontrolle
        self.rect.topleft = (x, y)

# Hindernisklasse für Kollisionserkennung
class Obstacle(pg.sprite.Sprite):
    def __init__(self, x, y, w, h, spriteGroup):
        pg.sprite.Sprite.__init__(self, spriteGroup)
        self.rect = pg.Rect(x, y, w, h)